            function HamburgerException (message) {
                this.message = "Hamburger exeption: " + message;
                // this.stack = (new Error()).stack;
            }

            // HamburgerException.prototype = Object.create(Error.prototype);
            
            function Hamburger(size, stuffing) {
               
                    if (size !==  Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE && size !== undefined) {
                            throw new HamburgerException("invalid size " + size.name);
                        };

                     if (size === undefined) {
                            throw new HamburgerException("no size given");
                        }; 

                    this.size = size;

                    if (stuffing === undefined) {
                            throw new HamburgerException("no stuffing given");  
                        };

                     if(stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_SALAD && 
                        stuffing !== Hamburger.STUFFING_POTATO){
                            throw new HamburgerException("invalid stuffing " + stuffing.name);
                        };

                    this.stuffing = stuffing;
                
                    this.toppings = [];
                };
    
            Hamburger.SIZE_SMALL = {name: 'SIZE_SMALL', price: 50, calories: 20};
            Hamburger.SIZE_LARGE = {name: 'SIZE_LARGE', price: 100, calories: 40};
            Hamburger.STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10, calories: 20};
            Hamburger.STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20, calories: 5};
            Hamburger.STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15, calories: 10};
            Hamburger.TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 20, calories: 5};
            Hamburger.TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 15, calories: 0};
    
           Hamburger.prototype.addTopping = function (topping) {
               var nameDobleTopping = this.toppings.find(function(element){return element === topping});

               if(topping !== Hamburger.TOPPING_MAYO && topping !== Hamburger.TOPPING_SPICE){
                        throw new HamburgerException("invalid topping " + topping.name);
               }

                //     if(this.toppings.forEach(function(item){
                //    item !== Hamburger.TOPPING_MAYO && item !== Hamburger.TOPPING_SPICE;})){
                //         throw new HamburgerException("invalid topping " + topping.name);
                //     } 
                    if (this.toppings.length > 2){
                        throw new HamburgerException("You cant add more topping")
                        }
                        
                    if (nameDobleTopping !== undefined){
                        throw new HamburgerException("double topping " + topping.name);
                    };
    
                    
               this.toppings.push(topping);
            };
       
    
            Hamburger.prototype.removeTopping = function (topping){
                var removeIndex= this.toppings.indexOf(topping);
                if (this.toppings.length === 0){
                    throw new HamburgerException("You try remove nothing");
                };
                                
                if (removeIndex === -1){
                    throw new HamburgerException("This kind of topping is absent");
                }; 

                if (topping !== Hamburger.TOPPING_MAYO && topping !==Hamburger.TOPPING_SPICE){
                    throw new HamburgerException("You try remove unknown topping");
                };

                this.toppings.splice(removeIndex, 1);
                    
                
            }
       
             Hamburger.prototype.getToppings = function (){
                 return this.toppings;
             }
    
            Hamburger.prototype.getSize = function () {
                return this.size;
            }
            
            Hamburger.prototype.getStuffing = function () {
                return this.stuffing;
            }
    
            Hamburger.prototype.calculatePrice = function (){
                var price = this.size.price + this.stuffing.price;
      
                  if (this.toppings !== undefined) {
                    for (topping of this.toppings) {
                        price += topping.price;
                        }
                    }
        
                return price;
            }
    
            Hamburger.prototype.calculateCalories = function (){
                var calories = this.size.calories + this.stuffing.calories;
      
                  if (this.toppings !== undefined) {
                    for (topping of this.toppings) {
                        calories += topping.calories;
                        }
                    }
                return calories;
            }
    
           
    try{
    //         // маленький гамбургер с начинкой из сыра
    var hamburger = new Hamburger( Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    console.log(hamburger);

    // добавка из майонеза
    hamburger.addTopping( Hamburger.TOPPING_MAYO);
    
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping( Hamburger.TOPPING_SPICE);
    
    // А сколько теперь стоит? 
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    
    // Проверить, большой ли гамбургер? 
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
    
    // не передали обязательные параметры
    // var h2 = new Hamburger(); 
    
    // // передаем некорректные значения, добавку вместо размера
    // var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 
    // // => HamburgerException: invalid size 'TOPPING_SPICE'
    
    // добавляем много добавок
    var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE); 
    h4.addTopping(Hamburger.STUFFING_CHEESE);
    h4.addTopping(Hamburger.TOPPING_MAYO); 
    // HamburgerException: duplicate topping 'TOPPING_MAYO'

} catch(err){
    console.error(err.message)
}

